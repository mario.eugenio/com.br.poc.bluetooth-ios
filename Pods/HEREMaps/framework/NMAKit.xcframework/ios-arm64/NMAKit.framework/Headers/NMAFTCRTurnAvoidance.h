/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>


/**
 * Represents turn avoidance options..
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMAFTCRTurnAvoidance : NSObject

/**
 * Initializes a `NMAFTCRTurnAvoidance` instance with default values.
 */
- (nonnull instancetype)init;

/**
 * Enable turn avoidance.
 */
@property (nonatomic) BOOL enabled;

/**
 * The minimal turn angle.
 */
@property (nonatomic) NSUInteger minimalTurnAngle;

/**
 * The penalty in seconds for a turn. The given time penalty will be applied if turn angle
 * is sharper then minimum turn angle.
 */
@property (nonatomic) NSUInteger penalty;

@end
