/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>


@class NMAGeoCoordinates;

NS_ASSUME_NONNULL_BEGIN


/**
 * Class that is used to specify road element using pvid.
 *
 * See also  `NMADynamicPenalty::addAllowedRoadElementIdentifier:` and
 * `NMADynamicPenalty::addPenaltyForRoadElementIdentifier:withDrivingDirection:`.
 */
@interface NMAPvidRoadElementIdentifier : NSObject

/**
 * Instances of this class should not be initialized directly
 */
- (instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (instancetype)new NS_UNAVAILABLE;

/**
 * Creates `NMAPvidRoadElementIdentifier` that can be used in online and offline route calculation.
 *
 * Provided `NMAGeoCoordinates` center should be located exactly on the road element geometry,
 * otherwise routing engine will not be able to find and match it to
 * the corresponding `NMARoadElement`. As the result offline routing will ignore this identifier.
 * If route is calculated only in online mode, use `initForOnlineRoutingWithPvid:` method instead.
 *
 * @param pvid  Permanent link id. See `NMARoadElement::permanentLinkId`.
 * @param center GeoCoordinate where road element or link is located. Is used to search
 *               `NMARoadElement` in map data.
 * @return `NMAPvidRoadElementIdentifier` object.
 */
- (instancetype)initWithPvid:(NSUInteger)pvid searchCenter:(NMAGeoCoordinates *)center
NS_SWIFT_NAME(init(pvid:searchCenter:)) NS_DESIGNATED_INITIALIZER;

/**
 * Creates `NMAPvidRoadElementIdentifier` that can be used only for online route calculation.
 *
 * To create `NMAPvidRoadElementIdentifier` that can be used in any type of connectivity mode
 * use `NMAPvidRoadElementIdentifier::initWithPvid:searchCenter:` initializer.
 * Identifier that was created using this method will be ignored by the routing
 * engine in offline route calculation.
 *
 * @note Route recalculation always happens in offline mode, therefore all allowed road element
 * identifiers that were created using this method will be ignored in route recalculation.
 *
 * @param pvid   Permanent link id.
 * @return `NMAPvidRoadElementIdentifier` object.
 */
- (instancetype)initForOnlineRoutingWithPvid:(NSUInteger)pvid
NS_SWIFT_NAME(initForOnlineRouting(pvid:)) NS_DESIGNATED_INITIALIZER;

/**
 *  Permanent link id. See `NMARoadElement::permanentLinkId`.
 */
@property (nonatomic, readonly) NSUInteger pvid;

/**
 * GeoCoordinate where road element or link is located.
 * Is used to search `NMARoadElement` in map data.
 */
@property (nonatomic, readonly) NMAGeoCoordinates *searchCenter;

@end

NS_ASSUME_NONNULL_END
