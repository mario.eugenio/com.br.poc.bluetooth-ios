/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

/**
 * Represents truck restriction type. For more details on truck restriction attibutes contact your
 * HERE representative.
 */
typedef NS_ENUM(NSUInteger, NMATruckRestrictionType) {

    /**
     * General truck restriction.
     */
    NMATruckRestrictionTypeTruckRestriction = 0,

    /**
     * Weight restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeWeightRestriction = 1,

    /**
     * Height restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeHeightRestriction = 2,

    /**
     * Length restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeLengthRestriction = 3,

    /**
     * Width restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeWidthRestriction = 4,

    /**
     * Weight per axle restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeWeightPerAxelRestriction = 5,

    /**
     * Kingpin-to-rear-axle restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeKpraLengthRestriction = 6,

    /**
     * Preferred route restriction.
     */
    NMATruckRestrictionTypePreferredRoute = 7,

    /**
     * Hazardous materials permit restriction.
     */
    NMATruckRestrictionTypeHazMatPermitRequired = 8,

    /**
     * Speed limit restriction.
     *
     * See also `getValue`
     */
    NMATruckRestrictionTypeSpeedLimit = 9,

    /**
     * Toll restriction.
     */
    NMATruckRestrictionTypeToll = 10,

    /**
     * Unknown restriction.
     */
    NMATruckRestrictionTypeUnknown = 11
};

/**
 * Represents truck hazardous material type.
 */
typedef NS_ENUM(NSUInteger, NMATruckRestrictionHazMat) {
    /**
     * None.
     */
    NMATruckRestrictionHazMatNone = 0,

    /**
     * Explosives.
     */
    NMATruckRestrictionHazMatExplosives = 1,
    /**
     * Gas.
     */
    NMATruckRestrictionHazMatGas = 2,

    /**
     * Flammable.
     */
    NMATruckRestrictionHazMatFlammable = 3,

    /**
     * Flammable solid/combustible.
     */
    NMATruckRestrictionHazMatFlammableSolidCombustible = 4,

    /**
     * Organic.
     */
    NMATruckRestrictionHazMatOrganic = 5,

    /**
     * Poison.
     */
    NMATruckRestrictionHazMatPoison = 6,

    /**
     * Radioactive.
     */
    NMATruckRestrictionHazMatRadioactive = 7,

    /**
     * Corrosive.
     */
    NMATruckRestrictionHazMatCorrosive = 8,

    /**
     * Other.
     */
    NMATruckRestrictionHazMatOther = 9,

    /**
     * Any hazardous material.
     */
    NMATruckRestrictionHazMatAny = 10,

    /**
     * Pih.
     */
    NMATruckRestrictionHazMatPih = 11,

    /**
     * Harmful for water.
     */
    NMATruckRestrictionHazMatHarmfulForWater = 12,

    /**
     * Explosive flammable.
     */
    NMATruckRestrictionHazMatExplosiveFlammable = 13,

    /**
     * Tunnel category B.
     */
    NMATruckRestrictionHazMatTunnelCategoryB = 14,

    /**
     * Tunnel category C.
     */
    NMATruckRestrictionHazMatTunnelCategoryC = 15,

    /**
     * Tunnel category D.
     */
    NMATruckRestrictionHazMatTunnelCategoryD = 16,

    /**
     * Tunnel category E.
     */
    NMATruckRestrictionHazMatTunnelCategoryE = 17,

    /**
     * Unknown.
     */
    NMATruckRestrictionHazMatUnknown = 18
};

/**
 * Represents weather condition restriction.
 */
typedef NS_ENUM(NSUInteger, NMATruckRestrictionWeatherType) {

    /**
     * None.
     */
    NMATruckRestrictionWeatherTypeNone = 0,

    /**
     * Rain.
     */
    NMATruckRestrictionWeatherTypeRain = 1,

    /**
     * Snow.
     */
    NMATruckRestrictionWeatherTypeSnow = 2,

    /**
     * Fog.
     */
    NMATruckRestrictionWeatherTypeFog = 3,

    /**
     * Unknown.
     */
    NMATruckRestrictionWeatherTypeUnknown = 4
};

/**
 * Represents restriction on a link or lane related to trucks with a specified number of
 * trailers.
 */
typedef NS_ENUM(NSUInteger, NMATruckRestrictionTrailerType) {
    /** None. */
    NMATruckRestrictionTrailerTypeNone = 0,

    /**
     * Truck with one or more trailers.
     * Deprecated as of SDK 3.17. Use `NMATruckRestrictionTrailerTruckWithOneOrMoreTrailers`
     * instead.
     */
    NMATruckRestrictionTrailerTypeValue1 DEPRECATED_ATTRIBUTE = 1,
    /**
     * Truck with two or more trailers.
     * Deprecated as of SDK 3.17. Use `NMATruckRestrictionTrailerTruckWithTwoOrMoreTrailers`
     * instead.
     */
    NMATruckRestrictionTrailerTypeValue2 DEPRECATED_ATTRIBUTE = 2,
    /**
     * Truck with three or more trailers.
     * Deprecated as of SDK 3.17. Use `NMATruckRestrictionTrailerTruckWithThreeOrMoreTrailers`
     * instead.
     */
    NMATruckRestrictionTrailerTypeValue3 DEPRECATED_ATTRIBUTE = 3,
    /**
     * Semi or tractor with 1 or more trailers.
     * Deprecated as of SDK 3.17. Use
     * `NMATruckRestrictionTrailerSemiOrTractorWithOneOrMoreTrailers` instead.
     */
    NMATruckRestrictionTrailerTypeValue4 DEPRECATED_ATTRIBUTE = 4,

    /** Truck with one or more trailers. **/
    NMATruckRestrictionTrailerTypeTruckWithOneOrMoreTrailers = 5,
    /** Truck with two or more trailers. **/
    NMATruckRestrictionTrailerTypeTruckWithTwoOrMoreTrailers = 6,
    /** Truck with three or more trailers. **/
    NMATruckRestrictionTrailerTypeTruckWithThreeOrMoreTrailers = 7,
    /** Semi or tractor with one or more trailers. **/
    NMATruckRestrictionTrailerTypeSemiOrTractorWithOneOrMoreTrailers = 8,
    /** Truck with no trailer. **/
    NMATruckRestrictionTrailerTypeTruckWithNoTrailer = 9,
    /** Truck with one trailer. **/
    NMATruckRestrictionTrailerTypeTruckWithOneTrailer = 10,
    /** Truck with two trailers. **/
    NMATruckRestrictionTrailerTypeTruckWithTwoTrailers = 11,
    /** Truck with three trailers. **/
    NMATruckRestrictionTrailerTypeTruckWithThreeTrailers = 12,
    /** Truck with four trailers. **/
    NMATruckRestrictionTrailerTypeTruckWithFourTrailers = 13,
    /** Straight truck with one or more trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithOneOrMoreTrailers = 14,
    /** Straight truck with two or more trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithTwoOrMoreTrailers = 15,
    /** Straight truck with three or more trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithThreeOrMoreTrailers = 16,
    /** Straight truck with or without trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithOrWithoutTrailers = 17,
    /** Straight truck with no trailer. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithNoTrailer = 18,
    /** Straight truck with one trailer. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithOneTrailer = 19,
    /** Straight truck with two trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithTwoTrailers = 20,
    /** Straight truck with three trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithThreeTrailers = 21,
    /** Straight truck with four trailers. **/
    NMATruckRestrictionTrailerTypeStraightTruckWithFourTrailers = 22,
    /** Semi truck with one or more trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithOneOrMoreTrailers = 23,
    /** Semi truck with two or more trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithTwoOrMoreTrailers = 24,
    /** Semi truck with three or more trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithThreeOrMoreTrailers = 25,
    /** Semi truck with or without trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithOrWithoutTrailers = 26,
    /** Semi truck with no trailer. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithNoTrailer = 27,
    /** Semi truck with one trailer. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithOneTrailer = 28,
    /** Semi truck with two trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithTwoTrailers = 29,
    /** Semi truck with three trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithThreeTrailers = 30,
    /** Semi truck with four trailers. **/
    NMATruckRestrictionTrailerTypeSemiTruckWithFourTrailers = 31,
    /**  Unknown. */
    NMATruckRestrictionTrailerTypeUnknown = 32
};

/**
 * Represents restriction on a link or lane related to trucks with a specified number of axles
 * or a distinct axle group.
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
typedef NS_ENUM(NSUInteger, NMATruckRestrictionAxle) {
    /** None. */
    NMATruckRestrictionAxleNone = 0,
    /** Two or more. */
    NMATruckRestrictionAxleTwoOrMore = 1,
    /** Three or more. */
    NMATruckRestrictionAxleThreeOrMore = 2,
    /** Four or more. */
    NMATruckRestrictionAxleFourOrMore = 3,
    /** Five or more. */
    NMATruckRestrictionAxleFiveOrMore = 4,
    /** Six or more. */
    NMATruckRestrictionAxleSixOrMore = 5,
    /** Single. */
    NMATruckRestrictionAxleSingle = 6,
    /** Tandem. */
    NMATruckRestrictionAxleTandem = 7,
    /** Triple. */
    NMATruckRestrictionAxleTriple = 8,
    /** Quad. */
    NMATruckRestrictionAxleQuad = 9,
    /** Quint. */
    NMATruckRestrictionAxleQuint = 10,
    /** Two. */
    NMATruckRestrictionAxleTwo = 11,
    /** Three. */
    NMATruckRestrictionAxleThree = 12,
    /** Four. */
    NMATruckRestrictionAxleFour = 13,
    /** Five. */
    NMATruckRestrictionAxleFive = 14,
    /** Six. */
    NMATruckRestrictionAxleSix = 15,
    /** Seven. */
    NMATruckRestrictionAxleSeven = 16,
    /** Unknown. */
    NMATruckRestrictionAxleUnknown = 17
};


/**
 * Gives information about the truck restriction, e.g. its type, restriction value etc.
 */
@interface NMATruckRestriction : NSObject

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Gets truck restrictoin type.
 */
@property (nonatomic, readonly) NMATruckRestrictionType truckRestrictionType;

/**
 * Gets truck hazardous materials restriction. Use `hasHazMatRestriction`
 * to check if there is any restriction of this type.
 */
@property (nonatomic, readonly) NMATruckRestrictionHazMat truckHazMatType;

/**
 * Gets the trailer type that is used to specify the occurrence of a restriction on a link or
 * lane related to trucks with a specified number of trailers. For more details on this
 * attibute contact your HERE representative.
 */
@property (nonatomic, readonly) NMATruckRestrictionTrailerType truckTrailerType;

/**
 * Gets truck axles restriction that is used to model signs specifying the number of axles that
 * are restricted. For more details on this attibute contact your HERE representative.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@property (nonatomic, readonly) NMATruckRestrictionAxle axleRestriction;

/**
 * Gets truck weather type restriction. Use `hasWeatherRestriction` to check if there is any
 * restriction of this type.
 */
@property (nonatomic, readonly) NMATruckRestrictionWeatherType truckWeatherType;

/**
 * Gets the value of `truckRestrictionType`. Use `hasValue` to check whether current
 * type has any value associated with it.
 * Value for each `truckRestrictionType` represents certain thing:
 * `NMATruckRestrictionTypeWeightRestriction` - maximum weight for truck in kg.
 * `NMATruckRestrictionTypeHeightRestriction` - maximum height for truck in cm.
 * `NMATruckRestrictionTypeLengthRestriction` - maximum length for truck in cm.
 * `NMATruckRestrictionTypeWidthRestriction` - maximum width for truck in cm.
 * `NMATruckRestrictionTypeWeightPerAxelRestriction` - maximum kg per axel.
 * `NMATruckRestrictionTypeKpraLengthRestriction` - kingpin to rear axle limit in cm.
 * `NMATruckRestrictionTypeSpeedLimit` - maximum speed in meters per second.
 *
 * Types below does not have any values associated with it:
 * `NMATruckRestrictionTypeTruckRestriction`
 * `NMATruckRestrictionTypeToll`
 * `NMATruckRestrictionTypePreferredRoute`
 * `NMATruckRestrictionTypeHazMatPermitRequired`
 */
@property (nonatomic, readonly) NSUInteger value;

/**
 * Checks whether `truckRestrictionType` has any value associated with it. Types such as
 * `NMATruckRestrictionTypeTruckRestriction`, `NMATruckRestrictionTypeToll`,
 * `NMATruckRestrictionTypePreferredRoute` or `NMATruckRestrictionTypeHazMatPermitRequired` do not
 * have value. All the other types should have value.
 */
-(BOOL)hasValue;

/**
 * Checks whether this truck restriction has additional hazardous materials restriction.
 * Use `truckHazMatType` to get type of hazmat restriction.
 */
-(BOOL)hasHazMatRestriction;

/**
 * Checks whether this truck restriction has trailer type. Use `truckTrailerType` to get type of
 * truck trailer. For more details on this attibute contact your HERE representative.
 */
-(BOOL)hasTrailerType;

/**
 * Checks whether this truck restriction has trailer type. Use `truckTrailerType` to get type of
 * truck trailer.
 *
 * DEPRECATED As of SDK 3.17. Use `hasTrailerType` instead.
 */
-(BOOL)hasTrailerRestriction DEPRECATED_ATTRIBUTE;

/**
 * Checks whether this truck restriction has axle restriction. Use `axleRestriction` to get truck
 * axle restriction.
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
-(BOOL)hasAxleRestriction;

/**
 * Checks whether this truck restriction has additional weather restriction.
 * Use `truckWeatherType` to get type of truck weather restriction.
 */
-(BOOL)hasWeatherRestriction;

@end
