/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

/**
 * All possible directions the lane leads to.
 *
 * A combination of the NMAFTCRLaneInformationDirection forms a direction bitmask.
 *
 * See also `NMAFTCRLaneInformation`
 */
typedef NS_OPTIONS(NSUInteger, NMAFTCRLaneInformationDirection) {
    /**
     * Indicates there are no markings on this lane and the direction is unknown.
     */
    NMAFTCRLaneInformationDirectionMaskUndefined NS_SWIFT_NAME(undefined) = 0,

    /**
     *Indicates that the direction of travel is straight ahead.
     */
    NMAFTCRLaneInformationDirectionMaskStraight NS_SWIFT_NAME(straight) = 1 << 0,

    /**
     * Indicates a slight right turn.
     */
    NMAFTCRLaneInformationDirectionMaskSlightlyRight NS_SWIFT_NAME(slightlyRight) = 1 << 1,

    /**
     * Indicates a right turn.
     */
    NMAFTCRLaneInformationDirectionMaskRight NS_SWIFT_NAME(right) = 1 << 2,

    /**
     * Indicates a sharp right turn.
     */
    NMAFTCRLaneInformationDirectionMaskSharpRight NS_SWIFT_NAME(sharpRight) = 1 << 3,

    /**
     * Indicates a left u-turn.
     */
    NMAFTCRLaneInformationDirectionMaskUTurnLeft NS_SWIFT_NAME(uTurnLeft) = 1 << 4,

    /**
     * Indicates a sharp left turn.
     */
    NMAFTCRLaneInformationDirectionMaskSharpLeft NS_SWIFT_NAME(sharpLeft) = 1 << 5,

    /**
     * Indicates a left turn.
     */
    NMAFTCRLaneInformationDirectionMaskLeft NS_SWIFT_NAME(left) = 1 << 6,

    /**
     * Indicates a slight left turn.
     */
    NMAFTCRLaneInformationDirectionMaskSlightlyLeft NS_SWIFT_NAME(slightlyLeft) = 1 << 7,

    /**
     * Indicates a right merge.
     */
    NMAFTCRLaneInformationDirectionMaskMergeRight NS_SWIFT_NAME(mergeRight) = 1 << 8,

    /**
     * Indicates a left merge.
     */
    NMAFTCRLaneInformationDirectionMaskMergeLeft NS_SWIFT_NAME(mergeLeft) = 1 << 9,

    /**
     * Indicates a merge of lanes.
     */
    NMAFTCRLaneInformationDirectionMaskMergeLanes NS_SWIFT_NAME(mergeLanes) = 1 << 10,

    /**
     * Indicates a right u-turn.
     */
    NMAFTCRLaneInformationDirectionMaskUTurnRight NS_SWIFT_NAME(uTurnRight) = 1 << 11,

    /**
     * Indicates a second right.
     */
    NMAFTCRLaneInformationDirectionMaskSecondRight NS_SWIFT_NAME(secondRight) = 1 << 12,

    /**
     * Indicates a second left.
     */
    NMAFTCRLaneInformationDirectionMaskSecondLeft NS_SWIFT_NAME(secondLeft) = 1 << 13
};

/**
 * Recommendation for taking the lane according to the lane connectivity.
 *
 * See also `NMAFTCRLaneInformation`
 */
typedef NS_ENUM(NSUInteger, NMAFTCRLaneInformationRecommendationState) {
    /**
     * The lane is not on the current route.
     */
    NMAFTCRLaneInformationRecommendationStateNotRecommended,

    /**
     * The lane is on the route at least up to the next decision point, but not for the whole
     * part of the route for which connectivity information is available.
     */
    NMAFTCRLaneInformationRecommendationStateRecommended
} NS_SWIFT_NAME(NMAFTCRLaneInformationRecommendation);


/**
 * Gives information about a lane, e.g. its type, direction and recommendation state.
 */
@interface NMAFTCRLaneInformation : NSObject

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * All the directions the lane leads to as an OR combination of one or more
 * `NMAFTCRLaneInformationDirection` values.
 *
 * @note directions can be used as a bitmask (e.g. for efficient indexing of image resources).
 */
@property (nonatomic, readonly) NMAFTCRLaneInformationDirection directions;

/**
 * Gets the matched direction to the current maneuver on the route.
 *
 * @note If there is no matched direction or maneuver is nil then
 * NMAFTCRLaneInformationDirectionMaskUndefined is returned.
 */
@property (nonatomic, readonly) NMAFTCRLaneInformationDirection matchedDirection;

/**
 * Gets the recommendation state of the lane according to the current route.
 */
@property (nonatomic, readonly) NMAFTCRLaneInformationRecommendationState recommendationState;

@end
