/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>


/**
 * Represents information about warning on the `NMAFTCRRoute`.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMAFTCRRouteWarning : NSObject

/**
* Defines types of warning categories.
*/
typedef NS_ENUM(NSInteger, NMAFTCRRouteWarningCategory) {
    /** Unknown. */
    NMAFTCRRouteWarningCategoryUnknown,
    /** Other. */
    NMAFTCRRouteWarningCategoryOther,
    /** Suspicious u-turn. */
    NMAFTCRRouteWarningCategorySuspiciousUTurn,
    /** Forbidden driving direction. */
    NMAFTCRRouteWarningCategoryForbiddenDrivingDirection,
    /** Forbidden access. */
    NMAFTCRRouteWarningCategoryForbiddenAccess,
    /** Leaving no through traffic zone. */
    NMAFTCRRouteWarningCategoryLeavingNoThroughTrafficZone,
    /** Illegal u-turn. */
    NMAFTCRRouteWarningCategoryIllegalUTurn,
    /** Gate traversal. */
    NMAFTCRRouteWarningCategoryGateTraversal,
    /** Illegal turn. */
    NMAFTCRRouteWarningCategoryIllegalTurn,
    /** Toll cost route link id not found in map. */
    NMAFTCRRouteWarningCategoryTollCostRouteLinkIdNotFoundInMap,
    /** Toll cost ferry links on route. */
    NMAFTCRRouteWarningCategoryTollCostFerryLinksOnRoute,
    /** Toll cost vehicle spec param mismatch. */
    NMAFTCRRouteWarningCategoryTollCostVehicleSpecParamMismatch,
    /** Toll cost vehicle near match. */
    NMAFTCRRouteWarningCategoryTollCostVehicleNearMatch,
    /** Optional way point. */
    NMAFTCRRouteWarningCategoryOptionalWayPoint,
    /** Driver rest time. */
    NMAFTCRRouteWarningCategoryDriverRestTime,
    /** Intermediate waypoint no through traffic. */
    NMAFTCRRouteWarningCategoryIntermediateWaypointNoThroughTraffic,
    /** Waiting for opening at waypoint. */
    NMAFTCRRouteWarningCategoryWaitingForOpeningAtWayPoint,
    /** Stopped due to timeout. */
    NMAFTCRRouteWarningCategoryStoppedDueToTimeout,
    /** Waiting to get link access. */
    NMAFTCRRouteWarningCategoryWaitingToGetLinkAccess,
    /** Road construction traversal. */
    NMAFTCRRouteWarningCategoryRoadConstructionTraversal,
    /** Arriving earlier than requested. */
    NMAFTCRRouteWarningCategoryArrivingEarlierThanRequested,
    /** Driver rest time violated. */
    NMAFTCRRouteWarningCategoryDriverRestTimeViolated,
    /** Driver rest time too late. */
    NMAFTCRRouteWarningCategoryDriverRestTimeTooLate,
    /** Toll pass violation. */
    NMAFTCRRouteWarningCategoryTollPassViolation,
    /** Environmental zone violation. */
    NMAFTCRRouteWarningCategoryEnvironmentalZoneViolation,
};

/**
 * Instances of this class should not be initialized directly.
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly.
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * The `NMAFTCRRouteWarningCategory` for this warning.
 */
@property (nonatomic, readonly) NMAFTCRRouteWarningCategory category;

/**
 * The message.
 */
@property (nonatomic, readonly, assign, nonnull) NSString *message;

/**
 * The truck restriction category code.
 */
@property (nonatomic, readonly, assign) NSUInteger truckRestrictionCategoryCode;

/**
 * The ISO 3166-1 (3-letter) country code.
 */
@property (nonatomic, readonly, assign, nonnull) NSString *isoCountryCode;

/**
 * The the route link index from `NMAFTCRRoute.routeLinks` array.
 */
@property (nonatomic, readonly, assign) NSUInteger routeLinkIndex;

/**
 * The duration in seconds.
 */
@property (nonatomic, readonly, assign) NSUInteger duration;

@end
