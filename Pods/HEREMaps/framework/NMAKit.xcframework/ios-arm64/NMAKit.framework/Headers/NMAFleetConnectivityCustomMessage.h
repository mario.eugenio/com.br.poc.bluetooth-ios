/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <NMAKit/NMAFleetConnectivityMessage.h>

#pragma mark - DEPRECATED

@class NMAGeoCoordinates;


/**
 * Represents incoming message with generic information from the operator.
 * The details of this message should be specified in the content dictionary.
 * DEPRECATED As of SDK 3.18.
 */
DEPRECATED_ATTRIBUTE
@interface NMAFleetConnectivityCustomMessage : NMAFleetConnectivityMessage

@end
