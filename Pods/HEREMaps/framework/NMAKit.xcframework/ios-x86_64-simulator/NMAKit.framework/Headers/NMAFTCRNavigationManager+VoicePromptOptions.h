/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <NMAKit/NMAFTCRNavigationManager.h>
#import <NMAKit/NMAFTCRManeuver.h>

/**
 * An interface for responding to voice guidance events sent by the `NMAFTCRNavigationManager`.
 */
@protocol NMAFTCRNavigationManagerAudioDelegate<NSObject>
@optional

/**
 * Called when the navigation manager has audio feedback to play.
 *
 * If the delegate returns NO, the pending feedback will not be played. If the delegate
 * returns YES or this method is not implemented, the feedback will be played. This
 * method will be called once for each voice instruction or other non-voice audio feedback
 * (e.g. speed warning beeps).
 *
 * This method can be used to play voice commands using a custom TTS (text to speech)
 * engine. To accomplish this you should write a new `NMAAudioOutput` class that plays text using
 * the custom engine. When this method is called create an instance of your custom
 * `NMAAudioOutput`, pass it to [NMAAudioManager playOutput:] and then return NO. This will ensure
 * your custom TTS output is correctly synced with non-voice audio feedback.
 *
 * @param navigationManager The `NMAFTCRNavigationManager` singleton.
 * @param text The text to be spoken.
 *
 * @return YES to have the voice feedback played by `NMAFTCRNavigationManager`, NO to prevent
 * `NMAFTCRNavigationManager` playing the voice feedback.
 */
- (BOOL)          navigationManager:(nonnull NMAFTCRNavigationManager*)navigationManager
    shouldPlayVoiceFeedbackWithText:(nullable NSString *)text
NS_SWIFT_NAME(navigationManager(_:shouldPlayVoiceFeedback:));

/**
 * Called when navigation manager will play audio feedback.
 *
 * @param navigationManager The `NMAFTCRNavigationManager` singleton.
 * @param text The text to be spoken.
 */
- (void)        navigationManager:(nonnull NMAFTCRNavigationManager*)navigationManager
    willPlayVoiceFeedbackWithText:(nullable NSString *)text
NS_SWIFT_NAME(navigationManager(_:willPlayVoiceFeedback:));

/**
 * Called when navigation manager has finished playing audio feedback.
 *
 * @param navigationManager The `NMAFTCRNavigationManager` singleton.
 * @param text The text that was spoken.
 */
- (void)       navigationManager:(nonnull NMAFTCRNavigationManager*)navigationManager
    didPlayVoiceFeedbackWithText:(nullable NSString *)text
NS_SWIFT_NAME(navigationManager(_:didPlayVoiceFeedback:));

@end

/**
 * Specifies at what times voice guidance prompts are triggered during navigation. For each
 * upcoming maneuver, an instruction prompt to perform the maneuver is always triggered.
 *
 * @note The distance-based conditions are evaluated first.
 * The time-based second. The time-based condition can be extended to take into
 * consideration either time or distance conditions.
 */
@interface NMAFTCRNavigationManager (NMAFTCRNavigationManagerVoicePromptOptions)

/**
 * Checks if voice prompts for certain maneuver is enabled. All voice prompts are enabled by
 * default.
 *
 * @param action Maneuver to be checked.
 *
 * @return True if voice prompts for this maneuver type is enabled or false otherwise.
 */
- (BOOL)isVoiceEnabledForManeuverAction:(NMAFTCRManeuverAction)action
NS_SWIFT_NAME(isVoiceEnabled(forManeuverAction:));

/**
 * Enables/disables voice prompts for certain maneuver.
 *
 * @param enabled State for maneuver voice prompt.
 * @param action Maneuver voice prompt to be enabled/disabled.
 */
- (void)setVoiceEnabled:(BOOL)enabled forManeuverAction:(NMAFTCRManeuverAction)action
NS_SWIFT_NAME(setVoiceEnabled(_:forManeuverAction:));

/**
 * Configures voice guidance rules. Controls minimal distance (in meters) from previous maneuver to
 * announce next maneuver. Default value is 20.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger minimumDistanceFromPreviousManeuver;

/**
 * Configures voice guidance rules. Controls maximum distance (in meters) from previous maneuver to
 * announce next maneuver. Default value is -1.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger maximumDistanceFromPreviousManeuver;

/**
 * Configures voice guidance rules. Controls minimal distance (in meters) to upcoming maneuver to
 * announce it. Default value is -1.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger minimumDistanceToUpcomingManeuver;

/**
 * Configures voice guidance rules. Controls maximum distance (in meters) to upcoming maneuver to
 * announce it. Default value is 60.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger maximumDistanceToUpcomingManeuver;

/**
 * Configures voice guidance rules. Controls minimal time (in seconds) from previous maneuver to
 * announce next maneuver. Default value is -1.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger minimumTimeIntervalFromPreviousManeuver;

/**
 * Configures voice guidance rules. Controls maximum time (in seconds) from previous maneuver to
 * announce next maneuver. Default value is -1.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger maximumTimeIntervalFromPreviousManeuver;

/**
 * Configures voice guidance rules. Controls minimal time (in seconds) to upcoming maneuver to
 * announce next maneuver. Default value is -1.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger minimumTimeIntervalToUpcomingManeuver;

/**
 * Configures voice guidance rules. Controls maximum time (in seconds) to upcoming maneuver to
 * announce next maneuver. Default value is 6.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 */
@property (nonatomic) NSInteger maximumTimeIntervalToUpcomingManeuver;

/**
 * Configures voice guidance rules. Controls the time based distance (in meters) before upcoming
 * maneuver. Default value is -1.
 *
 * @note Negative values, anything < 0 will avoid this rule.
 *
 * @note The distance-based conditions are evaluated first. The time-based second. The time-based
 * condition can be extended to take into consideration either time or distance conditions. That is
 * exactly what the time based distance option for.
 */
@property (nonatomic) NSInteger timeBasedDistanceToUpcomingManeuver;

/**
 * Resets all announcment rules back to default values.
 */
- (void)resetAnnouncementRules;

/**
 * Receives voice guidance event callbacks containing information about the current navigation
 * session.
 *
 * See also `NMAFTCRNavigationManagerAudioDelegate`.
 */
@property (nonatomic, weak, nullable) id<NMAFTCRNavigationManagerAudioDelegate> audioDelegate;

/**
 * Determines whether to enable navigation voice. If set to NO,
 * navigation will be running without voice commands.
 *
 * @note navigation voice is enabled by default.
 */
@property (nonatomic, getter = isVoiceEnabled) BOOL voiceEnabled;

@end
