/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>


/**
 * Represents waypoint vehicle restrictions options.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMAFTCRWaypointRestrictions : NSObject

/**
 * Initializes a `NMAFTCRWaypointRestrictions` instance with default values.
 */
- (nonnull instancetype)init;

/**
 * Radius in meters around a waypoint to ignore (vehicle type, admin truck,
 * time-based) restrictions.
 */
@property (nonatomic) NSUInteger ignoreRestrictionsRadius;

/**
 * Penalty factor which defines strictness to avoid restrictions around the waypoint in a range
 * of [0.0, 1.0].
 */
@property (nonatomic) double penaltyFactor;

/**
 * Set true if not only time based restrictions should be ignored.
 */
@property (nonatomic) BOOL ignoreAll;

@end
