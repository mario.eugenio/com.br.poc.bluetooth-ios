/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <NMAKit/NMAVenue3dBaseLocation.h>

@class NMAVenue3dLevel;
@class NMAVenue3dController;
@class NMAVenue3dVenue;



/**
 * Defines an indoor location used as either start or endpoint in indoor routing.
 *
 * See also `NMAVenue3dSpaceLocation`
 *
 * See also `NMAVenue3dOutdoorLocation`
 *
 * See also `NMAVenue3dRoutingController`
 */

@interface NMAVenue3dLevelLocation : NMAVenue3dBaseLocation

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Constructs an `NMAVenue3dLevelLocation` object.
 *
 * @param level A Level object
 * @param geoCoordinates A geocoordinate object
 * @param controler A `NMAVenue3dController` object representing a venue of the location
 */
+ (nullable instancetype)levelLocationOnLevel:(nonnull NMAVenue3dLevel *)level
                            withGeoCoordinate:(nonnull NMAGeoCoordinates *)geoCoordinates
                                      inVenue:(nonnull NMAVenue3dController *)controler
NS_SWIFT_UNAVAILABLE("Use corresponding instance initializer");

/**
 * Constructs an `NMAVenue3dLevelLocation` object.
 *
 * @param venue A `NMAVenue3dVenue` object representing a venue of the location
 * @param level A `NMAVenue3dLevel` object
 * @param geoCoordinates A geocoordinate object
 */
+ (nullable instancetype)levelLocationInVenue:(nonnull NMAVenue3dVenue *)venue
                                      onLevel:(nonnull NMAVenue3dLevel *)level
                              atGeoCoordinate:(nonnull NMAGeoCoordinates *)geoCoordinates
NS_SWIFT_UNAVAILABLE("Use corresponding instance initializer");

/**
 * Constructs an `NMAVenue3dLevelLocation` object.
 *
 * @param level A `NMAVenue3dLevel` object
 * @param geoCoordinates A geocoordinate object
 * @param venue A `NMAVenue3dController` object representing a venue of the location
 */
- (nullable instancetype)initOnLevel:(nonnull NMAVenue3dLevel *)level
                   withGeoCoordinate:(nonnull NMAGeoCoordinates *)geoCoordinates
                             inVenue:(nonnull NMAVenue3dController *)venue
NS_SWIFT_NAME(init(on:at:in:));

/**
 * Constructs an `NMAVenue3dLevelLocation` object.
 *
 * @param venue A `NMAVenue3dVenue` object representing a venue of the location
 * @param level A `NMAVenue3dLevel` object
 * @param geoCoordinates A geocoordinate object
 */
- (nullable instancetype)initInVenue:(nonnull NMAVenue3dVenue *)venue
                             onLevel:(nonnull NMAVenue3dLevel *)level
                     atGeoCoordinate:(nonnull NMAGeoCoordinates *)geoCoordinates
NS_SWIFT_NAME(init(in:on:at:));

@end
