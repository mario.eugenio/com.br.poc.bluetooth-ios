/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

@class NMAGeoCoordinates;

/**
 * A completion block used to handle the result of a `NMARMERouteMatcher` request.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 *
 * @param matchedCoordinates The array of possible matched coordinates. Can be nil if error occurred.
 * @param error  A routing calculation error. `nil` if there was no error during calculation.
 *               `NSError` object contains error code and description message.
 */
typedef void(^NMARMERouteMatcherResultBlock)(NSArray<NMAGeoCoordinates *> * _Nullable matchedCoordinates,
                                             NSError* _Nullable error);


/**
 * `NMARMERouteMatcher` is class used to compute most probable road waypoints from raw GPS data.
 *
 * @note This functionality requires Internet connection and can be used only in online mode.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMARMERouteMatcher : NSObject

/**
 * Matches route to the given list of trace coordinates. Result
 * block will be executed on the main thread.
 *
 * @note Traces size should be no more than 1500 geo coordinates. Developer can split
 * original traces into several chunks to be able to match longer route. Currently only car
 * trace coordinates are supported.
 *
 * @param traceCoordinates The list of trace coordinates.
 * @param completion The completion block that is used to pass the result.
 *
 * @return The id for the match route task.
 */
- (NSInteger)matchRouteFromTraces:(nonnull NSArray<NMAGeoCoordinates *> *)traceCoordinates
                  completionBlock:(nonnull NMARMERouteMatcherResultBlock)completion
NS_SWIFT_NAME(matchRoute(fromTraces:_:));

/**
 * Matches route to the given list of trace coordinates. Result
 * block will be executed on the main thread.
 *
 * @note Traces size should be no more than 1500 geo coordinates. Developer can split
 * original traces into several chunks to be able to match longer route. Currently only car
 * trace coordinates are supported.
 *
 * @param pathToGPXFile The gpx file location in filesystem.
 * @param completion The completion block that is used to pass the result.
 *
 * @return The id for the match route task.
 */
- (NSInteger)matchRouteFromGPXFileAtPath:(nonnull NSURL *)pathToGPXFile
                         completionBlock:(nonnull NMARMERouteMatcherResultBlock)completion
NS_SWIFT_NAME(matchRoute(fromGPXFileAtPath:_:));

/**
 * Cancels ongoing route calculation task.
 *
 * @param taskId The route calculation identifier.
 */
- (void)cancel:(NSInteger)taskId;

@end
