/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

@class NMAGeoCoordinates;
@class NMAFTCRLaneInformation;


/**
 * Represents information about maneuver on the `NMAFTCRRoute`.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMAFTCRManeuver : NSObject

/**
* Defines types of actions for a NMAFTCRManeuver.
*/
typedef NS_ENUM(NSInteger, NMAFTCRManeuverAction) {
    /** Undefined. */
    NMAFTCRManeuverActionUndefined = 0,
    /** Depart. */
    NMAFTCRManeuverActionDepart,
    /** Depart airport. */
    NMAFTCRManeuverActionDepartAirport,
    /** Arrive. */
    NMAFTCRManeuverActionArrive,
    /** Arrive airport. */
    NMAFTCRManeuverActionArriveAirport,
    /** Arrive left. */
    NMAFTCRManeuverActionArriveLeft,
    /** Arrive right. */
    NMAFTCRManeuverActionArriveRight,
    /** Left loop. */
    NMAFTCRManeuverActionLeftLoop,
    /** Left u-turn. */
    NMAFTCRManeuverActionLeftUTurn,
    /** Sharp left turn. */
    NMAFTCRManeuverActionSharpLeftTurn,
    /** Left turn. */
    NMAFTCRManeuverActionLeftTurn,
    /** Slight left turn. */
    NMAFTCRManeuverActionSlightLeftTurn,
    /** Continue on. */
    NMAFTCRManeuverActionContinueOn,
    /** Slight right turn. */
    NMAFTCRManeuverActionSlightRightTurn,
    /** Right turn. */
    NMAFTCRManeuverActionRightTurn,
    /** Sharp right turn. */
    NMAFTCRManeuverActionSharpRightTurn,
    /** Right u-turn. */
    NMAFTCRManeuverActionRightUTurn,
    /** Right loop. */
    NMAFTCRManeuverActionRightLoop,
    /** Left exit. */
    NMAFTCRManeuverActionLeftExit,
    /** Right exit. */
    NMAFTCRManeuverActionRightExit,
    /** Left ramp. */
    NMAFTCRManeuverActionLeftRamp,
    /** Right ramp. */
    NMAFTCRManeuverActionRightRamp,
    /** Left fork. */
    NMAFTCRManeuverActionLeftFork,
    /** Middle fork. */
    NMAFTCRManeuverActionMiddleFork,
    /** Right fork. */
    NMAFTCRManeuverActionRightFork,
    /** Left merge. */
    NMAFTCRManeuverActionLeftMerge,
    /** Right merge. */
    NMAFTCRManeuverActionRightMerge,
    /** Name change. */
    NMAFTCRManeuverActionNameChange,
    /** Traffic circle. */
    NMAFTCRManeuverActionTrafficCircle,
    /** Ferry. */
    NMAFTCRManeuverActionFerry,
    /** Left roundabout exit 1. */
    NMAFTCRManeuverActionLeftRoundaboutExit1,
    /** Left roundabout exit 2. */
    NMAFTCRManeuverActionLeftRoundaboutExit2,
    /** Left roundabout exit 3. */
    NMAFTCRManeuverActionLeftRoundaboutExit3,
    /** Left roundabout exit 4. */
    NMAFTCRManeuverActionLeftRoundaboutExit4,
    /** Left roundabout exit 5. */
    NMAFTCRManeuverActionLeftRoundaboutExit5,
    /** Left roundabout exit 6. */
    NMAFTCRManeuverActionLeftRoundaboutExit6,
    /** Left roundabout exit 7. */
    NMAFTCRManeuverActionLeftRoundaboutExit7,
    /** Left roundabout exit 8. */
    NMAFTCRManeuverActionLeftRoundaboutExit8,
    /** Left roundabout exit 9. */
    NMAFTCRManeuverActionLeftRoundaboutExit9,
    /** Left roundabout exit 10. */
    NMAFTCRManeuverActionLeftRoundaboutExit10,
    /** Left roundabout exit 11. */
    NMAFTCRManeuverActionLeftRoundaboutExit11,
    /** Left roundabout exit 12. */
    NMAFTCRManeuverActionLeftRoundaboutExit12,
    /** Right roundabout exit 1. */
    NMAFTCRManeuverActionRightRoundaboutExit1,
    /** Right roundabout exit 2. */
    NMAFTCRManeuverActionRightRoundaboutExit2,
    /** Right roundabout exit 3. */
    NMAFTCRManeuverActionRightRoundaboutExit3,
    /** Right roundabout exit 4. */
    NMAFTCRManeuverActionRightRoundaboutExit4,
    /** Right roundabout exit 5. */
    NMAFTCRManeuverActionRightRoundaboutExit5,
    /** Right roundabout exit 6. */
    NMAFTCRManeuverActionRightRoundaboutExit6,
    /** Right roundabout exit 7. */
    NMAFTCRManeuverActionRightRoundaboutExit7,
    /** Right roundabout exit 8. */
    NMAFTCRManeuverActionRightRoundaboutExit8,
    /** Right roundabout exit 9. */
    NMAFTCRManeuverActionRightRoundaboutExit9,
    /** Right roundabout exit 10. */
    NMAFTCRManeuverActionRightRoundaboutExit10,
    /** Right roundabout exit 11. */
    NMAFTCRManeuverActionRightRoundaboutExit11,
    /** Right roundabout exit 12. */
    NMAFTCRManeuverActionRightRoundaboutExit12,
    /** Invalid. */
    NMAFTCRManeuverActionInvalid = -1
};

/**
* Defines types of directions for a NMAFTCRManeuver.
*/
typedef NS_ENUM(NSInteger, NMAFTCRManeuverDirection) {
    /** Undefined. */
    NMAFTCRManeuverDirectionUndefined = 0,
    /** Forward. */
    NMAFTCRManeuverDirectionForward,
    /** Bear right. */
    NMAFTCRManeuverDirectionBearRight,
    /** Light right. */
    NMAFTCRManeuverDirectionLightRight,
    /** Right. */
    NMAFTCRManeuverDirectionRight,
    /** Hard right. */
    NMAFTCRManeuverDirectionHardRight,
    /** U-turn right. */
    NMAFTCRManeuverDirectionUTurnRight,
    /** Bear left. */
    NMAFTCRManeuverDirectionBearLeft,
    /** Light left. */
    NMAFTCRManeuverDirectionLightLeft,
    /** Left. */
    NMAFTCRManeuverDirectionLeft,
    /** Hard left. */
    NMAFTCRManeuverDirectionHardLeft,
    /** U-turn left. */
    NMAFTCRManeuverDirectionUTurnLeft,
    /** Invalid. */
    NMAFTCRManeuverDirectionInvalid = -1
};

/**
 * Instances of this class should not be initialized directly.
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly.
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * The `NMAGeoCoordinates` of the maneuver.
 */
@property (nonatomic, readonly, strong, nullable) NMAGeoCoordinates *position;

/**
 * The instruction of the maneuver.
 */
@property (nonatomic, readonly, strong, nullable) NSString *instruction;

/**
 * The time to the next maneuver in seconds.
 */
@property (nonatomic, readonly, assign) NSUInteger travelTime;

/**
 * The length to the next maneuver in meters.
 */
@property (nonatomic, readonly, assign) NSUInteger length;

/**
 * The `NMAFTCRManeuverAction` to take for the maneuver.
 *
 * @note If the action for the maneuver is undefined, attempts to read this
 * property will return NMAFTCRManeuverActionUndefined.
 */
@property (nonatomic, readonly) NMAFTCRManeuverAction action;

/**
 * The `NMAFTCRManeuverDirection` to take for the maneuver.
 *
 * @note If the direction for the maneuver is undefined, attempts to read this
 * property will return NMAFTCRManeuverDirectionUndefined.
 */
@property (nonatomic, readonly) NMAFTCRManeuverDirection direction;

/**
 * The `NSArray` of `NMAFTCRLaneInformation` represents the lanes of the `NMAFTCRManeuver`.
 */
@property (nonatomic, readonly, nonnull) NSArray<NMAFTCRLaneInformation *> *lanes;

/**
* The `NSArray` of `NSString` represents the road names for this maneuver.
*/
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *roadNames;

/**
* The `NSArray` of `NSString` represents the next road names after this maneuver.
*/
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *nextRoadNames;

@end
