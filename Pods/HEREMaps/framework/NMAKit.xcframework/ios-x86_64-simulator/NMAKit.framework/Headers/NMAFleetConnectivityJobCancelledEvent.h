/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <NMAKit/NMAFleetConnectivityEvent.h>

#pragma mark - DEPRECATED


/**
 * Represents event used to inform the operator that the ongoing job was cancelled.
 * DEPRECATED As of SDK 3.18.
 */
DEPRECATED_ATTRIBUTE
@interface NMAFleetConnectivityJobCancelledEvent : NMAFleetConnectivityEvent

@end
