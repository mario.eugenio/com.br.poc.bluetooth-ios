/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

@class NMAWaypoint;
@class NMAFTCRRouteOptions;


/**
 * Represents plan that will be used for route calculation.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMAFTCRRoutePlan : NSObject

/**
 * Instances of this class should not be initialized directly.
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly.
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Initializes an `NMAFTCRRoutePlan` instance with specified waypoints and overlay.
 * The default `FTCRRouteOptions` are set.
 *
 * @param waypoints Should contain at least two waypoins.
  *
 * @return The `NMAFTCRRoutePlan` instance.
 */
- (nonnull instancetype)initWithWaypoints:(nonnull NSArray<NMAWaypoint *> *)waypoints
NS_SWIFT_NAME(init(waypoints:));

/**
 * Initializes an `NMAFTCRRoutePlan` instance with specified waypoints, overlay and options.
 *
 * @param waypoints Should contain at least two waypoins.
 * @param options Route options.
 *
 * @return The `NMAFTCRRoutePlan` instance.
 */
- (nonnull instancetype)initWithWaypoints:(nonnull NSArray<NMAWaypoint *> *)waypoints
                                  options:(nonnull NMAFTCRRouteOptions *)options
NS_SWIFT_NAME(init(waypoints:options:));

/**
 * Overlay name for `NMAFTCRRouter` request.
 */
@property (nullable, nonatomic, strong) NSString *overlay;

/**
 * Waypoints for `NMAFTCRRouter` request, must contain at least two waypoins.
 */
@property (nonnull, nonatomic, strong) NSArray<NMAWaypoint *> *waypoints;

/**
 * Route options for `NMAFTCRRouter` request.
 */
@property (nonnull, nonatomic, strong) NMAFTCRRouteOptions *options;

@end
