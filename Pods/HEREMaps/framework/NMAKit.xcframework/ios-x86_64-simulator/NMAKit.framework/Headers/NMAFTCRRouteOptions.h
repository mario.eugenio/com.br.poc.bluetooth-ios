/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>


@class NMAGeoBoundingBox;
@class NMAFTCRTurnAvoidance;
@class NMAFTCRWaypointRestrictions;

/**
 * Represents values describing different routing types.
 */
typedef NS_ENUM(NSUInteger, NMAFTCRRoutingType) {
    /**
     * Search for the fastest route, taking road conditions and restrictions into account
     * (minimizes travel time).
     */
    NMAFTCRRoutingTypeFastest,
    /**
     * Search for the shortest route, taking road conditions and restrictions into account
     * (minimizes travel distance).
     */
    NMAFTCRRoutingTypeShortest
};

/**
 * Represents values describing different transport modes.
 * The mode of transportation a person will be using to travel a route (e.g. a car).
 */
typedef NS_ENUM(NSUInteger, NMAFTCRTransportMode) {
    /**
     * A car is being used as the mode of transportation.
     */
    NMAFTCRTransportModeCar,
    /**
     * A truck is being used as the mode of transportation.
     */
    NMAFTCRTransportModeTruck,
    /**
     * Walking is being used as the mode of transportation.
     */
    NMAFTCRTransportModePedestrian,
    /**
     * A scooter is being used as the mode of transportation.
     */
    NMAFTCRTransportModeScooter,
    /**
     * A bike is being used as the mode of transportation.
     */
    NMAFTCRTransportModeBike,
    /**
     * A bus is being used as the mode of transportation.
     */
    NMAFTCRTransportModeBus
};

/**
 * Represents functional classes.
 */
typedef NS_ENUM(NSUInteger, NMAFTCRFunctionalClass) {
    /**
     * Road functional class 1.
     */
    NMAFTCRFunctionalClassFC1,
    /**
     * Road functional class 2.
     */
    NMAFTCRFunctionalClassFC2,
    /**
     * Road functional class 3.
     */
    NMAFTCRFunctionalClassFC3,
    /**
     * Road functional class 4.
     */
    NMAFTCRFunctionalClassFC4,
    /**
     * Road functional class 5.
     */
    NMAFTCRFunctionalClassFC5,
    /**
     * Virtual Connection.
     */
    NMAFTCRFunctionalClassVC,
};

/**
 * Represents usage mode of private streets for routing.
 */
typedef NS_ENUM(NSUInteger, NMAFTCRPrivateStreetUsageMode) {
    /**
     * Access is allowed if there is a waypoint in the private street.
     */
    NMAFTCRPrivateStreetUsageModeDefault,
    /**
     * Always allow.
     */
    NMAFTCRPrivateStreetUsageModeAllowed,
    /**
     * Always forbidden.
     */
    NMAFTCRPrivateStreetUsageModeForbidden
};

/**
 * Represents different options of routing avoidance.
 */
typedef NS_ENUM(NSUInteger, NMAFTCRRouteAvoidance) {
    /**
     * No avoidance options.
     */
    NMAFTCRRouteAvoidanceNone,
    /**
     * Default avoidance options.
     */
    NMAFTCRRouteAvoidanceDefault,
    /**
     * Always avoid.
     */
    NMAFTCRRouteAvoidanceAvoid,
    /**
     * Soft exclude.
     */
    NMAFTCRRouteAvoidanceSoftExclude,
    /**
     * Strict exclude.
     */
    NMAFTCRRouteAvoidanceStrictExclude
};


/**
 *
 * Represents a model of the parameters required for one FTCR route
 * calculation, encapsulating parameters such as:
 *
 * - The `NMAFTCRRoutingType`
 * - The `NMAFTCRTransportMode`
 * and etc.
 *
 * IMPORTANT: This is a Beta API, and is subject to change without notice.
 */
@interface NMAFTCRRouteOptions : NSObject

/**
 * Initializes a `NMAFTCRRouteOptions` instance with default values.
 */
- (nonnull instancetype)init;

/**
 * Initializes a `NMAFTCRRouteOptions` instance with the specified routing
 * parameters.
 *
 * @param routingType - A routing type (see `NMAFTCTRoutingType`)
 * @param transportMode - A transport mode (see `NMAFTCRTransportMode`)
 * @return The `NMAFTCRRouteOptions` object, or nil if initialization failed
 */
- (nonnull instancetype)initWithRoutingType:(NMAFTCRRoutingType)routingType
                              transportMode:(NMAFTCRTransportMode)transportMode
NS_SWIFT_NAME(init(routingType:transportMode:));

/**
 * The `NMAFTCRRoutingType` for the `NMAFTCRRouteOptions`.
 *
 * @note The default value is NMAFTCRRoutingTypeFastest.
 */
@property (nonatomic) NMAFTCRRoutingType routingType;

/**
 * The `NMAFTCRTransportMode` for the `NMAFTCRRouteOptions`.
 *
 * @note The default value is NMAFTCRTransportModeCar.
 */
@property (nonatomic) NMAFTCRTransportMode transportMode;

/**
 * Indicates whether traffic information should be considered for route calculation.
 */
@property (nonatomic, getter=isUseTraffic) BOOL useTraffic;

/**
 * Left turns avoidance settings.
 */
@property (nonatomic, nonnull) NMAFTCRTurnAvoidance *leftTurnAvoidance;

/**
* Right turns avoidance settings.
*/
@property (nonatomic, nonnull) NMAFTCRTurnAvoidance *rightTurnAvoidance;

/**
 * U-turns avoidance settings.
 *
 * @note The minimal turn angle is not supported for u-turns.
 */
@property (nonatomic, nonnull) NMAFTCRTurnAvoidance *uTurnAvoidance;

/**
 * Indicates whether or not u-turning on the link directly where the waypoint was reached.
 */
@property (nonatomic, getter=isUTurnAtWaypointEnabled) BOOL uTurnAtWaypointEnabled;

/**
 * Waypoint vehicle restricions options.
 */
@property (nonatomic, nonnull) NMAFTCRWaypointRestrictions *waypointRestrictions;

/**
 * Adds the area avoided for routing.
 */
- (void)addAvoidArea:(nonnull NMAGeoBoundingBox *)area;

/**
 * Returns all areas avoided for routing.
 */
@property (nonatomic, readonly, nonnull) NSArray<NMAGeoBoundingBox *> *avoidAreas;

/**
 * Indicates usage mode of private streets for routing.
 */
@property (nonatomic) NMAFTCRPrivateStreetUsageMode privateStreetUsageMode;

/**
 * Gets speed for road functional class and speed category.
 *
 * @param functionalClass
 *         The desired `NMAFTCRFunctionalClass`.
 * @param speedCategory
 *         The desired speed category [1..8].
 * @return Speed in kilometers per hour.
 */
- (NSUInteger)getSpeedForFunctionalClass:(NMAFTCRFunctionalClass)functionalClass
                           speedCategory:(NSUInteger)speedCategory
NS_SWIFT_NAME(getSpeed(forFunctionalClass:speedCategory:));

/**
 * Sets speed for road functional class and speed category.
 *
 * @param speed
 *         The desired speed in kilometers per hour.
 *         If set to 0, the default speed will be used.
 * @param functionalClass
 *         The desired `NMAFTCRFunctionalClass`.
 * @param speedCategory
 *         The desired speed category [1..8].
 */
-   (void)setSpeed:(NSUInteger)speed
forFunctionalClass:(NMAFTCRFunctionalClass)functionalClass
     speedCategory:(NSUInteger)speedCategory
NS_SWIFT_NAME(setSpeed(_:forfunctionalClass:speedCategory:));

/**
 * Adds custom parameter to the route request.
 *
 * @param name Name of the parameter.
 * @param value Value of the parameter.
 */
- (void)addParameter:(nonnull NSString *)name value:(nonnull NSString *)value
NS_SWIFT_NAME(addParameter(name:value:));

/**
 * The limited vehicle weight in metric tons.
 *
 * @note The default value is 0 (no vehicle weight limit).
 */
@property (nonatomic) float limitedVehicleWeight;

/**
 * The vehicle weight per axle in metric tons.
 *
 * @note The default value is 0 (unspecified).
 */
@property (nonatomic) float weightPerAxle;

/**
 * The vehicle height in meters.
 *
 * @note The default value is 0 (unspecified).
 */
@property (nonatomic) float vehicleHeight;

/**
 * The vehicle width in meters.
 *
 * @note The default value is 0 (unspecified).
 */
@property (nonatomic) float vehicleWidth;

/**
 * The vehicle length in meters.
 *
 * @note The default value is 0 (unspecified).
 */
@property (nonatomic) float vehicleLength;

/**
 * Indicates avoidance mode for motorways.
 */
@property (nonatomic) NMAFTCRRouteAvoidance motorwaysAvoidance;

/**
 * Indicates avoidance mode for toll roads.
 */
@property (nonatomic) NMAFTCRRouteAvoidance tollRoadsAvoidance;

/**
 * Indicates avoidance mode for boat ferries.
 */
@property (nonatomic) NMAFTCRRouteAvoidance boatFerriesAvoidance;

/**
 * Indicates avoidance mode for rail ferries.
 */
@property (nonatomic) NMAFTCRRouteAvoidance railFerriesAvoidance;

/**
 * Indicates avoidance mode for tunnels.
 */
@property (nonatomic) NMAFTCRRouteAvoidance tunnelsAvoidance;

/**
 * The desired departure time.
 *
 * @note Only one of the properties of route time can be set at a time. Setting this property to
 *       a nonzero value resets the `NMAFTCRRouteOptions.arrivalTime`
 */
@property (nonatomic, strong, nullable) NSDate *departureTime;

/**
 * The desired arrival time.
 *
 * @note Only one of the properties of route time can be set at a time. Setting this property to
 *       a nonzero value resets the `NMAFTCRRouteOptions.departureTime`
 */
@property (nonatomic, strong, nullable) NSDate *arrivalTime;

/**
 * Custom parameters of the route request.
 */
@property (nonatomic, nonnull, readonly) NSDictionary<NSString *, NSString *> *parameters;

@end
