/*
 * Copyright (c) 2011-2021 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <NMAKit/NMAFTCRNavigationManager.h>
#import <NMAKit/NMAFTCRManeuver.h>


/**
 * This category can be used to specify different settings for `NMAFTCRNavigationManager`.
 */
@interface NMAFTCRNavigationManager (NMAFTCRNavigationManagerOptions)

/**
 * Sets the distance threshold that will be used to consider that stop waypoint and/or
 * destination has been reached. This value will be used for all subsequent `startWithRoute:`.
 *
 * @note Default value is 10 meters.
 *
 * @param distance The distance from the current position to the upcoming stopover in
 * meters when SDK should trigger `navigationManager:didReachStopover:` callback.
 * Setting distance to 0 will be ignored.
 */
- (void)setDistanceToTriggerStopoverReached:(NSUInteger)distance
NS_SWIFT_NAME(setDistanceToTriggerStopoverReached(distance:));

@end
